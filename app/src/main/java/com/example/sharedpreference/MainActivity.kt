package com.example.sharedpreference

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        read()
    }
    private fun init(){
        sharedPreferences = getSharedPreferences("user", MODE_PRIVATE)
    }

    fun save(view: View){
        val email = emailET.text.toString()
        val firstName = firstNameET.text.toString()
        val lastName = lastNameET.text.toString()
        val age = ageET.text.toString()
        val address = addressET.text.toString()
        val edit = sharedPreferences.edit()


        if(email.isNotEmpty() && firstName.isNotEmpty() && lastName.isNotEmpty() && address.isNotEmpty() && age.isNotEmpty()){
            edit.putString("firstName", firstName)
            edit.putString("lastName", lastName)
            edit.putString("email", email)
            edit.putString("Age", age)
            edit.putString("address", address)
            edit.apply()
            Toast.makeText(this, "Information saved", Toast.LENGTH_SHORT).show()
        }else
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
    }

    private fun read(){
        val email = sharedPreferences.getString("email", "")
        val firstName = sharedPreferences.getString("firstName", "")
        val lastName = sharedPreferences.getString("lastName", "")
        val age = sharedPreferences.getString("Age", "")
        val address = sharedPreferences.getString("address", "")

        emailET.setText(email)
        firstNameET.setText(firstName)
        lastNameET.setText(lastName)
        ageET.setText(age)
        addressET.setText(address)
    }
}